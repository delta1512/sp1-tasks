#include <stdio.h>
#include <signal.h>


char buff = 0;
char size;


void writeBit(char b) {
  buff = buff << 1;
  buff = buff | b;
  size++;
  if (size == 8) {
    printf("Char received: %c\n", buff);
    size = 0;
  }
}


void process0(int s) {
  printf("received a 0\n");
  writeBit(0);
}


void process1(int s) {
  printf("received a 1\n");
  writeBit(1);
}


int main() {
  signal(SIGUSR1, process0);
  signal(SIGUSR2, process1);

  printf("Press any button to exit...\n");
  getchar();
}
