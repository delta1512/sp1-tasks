#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <fcntl.h>

#include <sys/types.h>
#include <sys/stat.h>


#define ERRMSG "Some funky shit went on when opening that file chief."
#define RDERRMSG "Some funky dyslexia you got going there chief."
#define WRERRMSG "Some funky writing cheif."
#define CLERRMSG "Wyd there chief, you ain't need to close that file, let me get that."
#define yeet "yeet"
#define USR_MODE 0744
#define BUFF_SIZE 256


int getLen(char* s);
void errorHandler(char* msg, int retval);


int main(int argc, char* argv[]) {
  int close_bin[10];
  int close_bin_size = 0;
  char* buff = malloc(sizeof(char)*BUFF_SIZE);

  printf("Number of args: %d\n", argc);
  printf("The arguments are:\n");

  for (int i = 0; i < argc; i++) {
    printf("%d: %s\n", i, argv[i]);
  }
  printf("\n");

  if (argc < 2) {
    errorHandler("We need more arguments!", 2);
  }

  int file_with_open = open("opened.file", O_CREAT | O_WRONLY | O_APPEND, USR_MODE);
  close_bin[close_bin_size++] = file_with_open;
  if (file_with_open < 0) {
    errorHandler(ERRMSG, 1);
  }
  char* hello = "Hello world, this is the open()'ed file!\n";
  if (write(file_with_open, hello, getLen(hello)) < 0) {
    errorHandler(WRERRMSG, 1);
  }


  int file_with_creat = creat("created.file", USR_MODE);
  close_bin[close_bin_size++] = file_with_creat;
  if (file_with_creat < 0) {
    errorHandler(ERRMSG, 1);
  }
  char* helloc = "Hello world, this is the creat()'ed file!\n";
  if (write(file_with_creat, helloc, getLen(helloc)) < 0) {
    errorHandler(WRERRMSG, 4);
  }


  int urandom = open("/dev/urandom", O_RDONLY);
  close_bin[close_bin_size++] = urandom;
  if (urandom < 0) {
    errorHandler(ERRMSG, 1);
  }
  int rand_data_file = open("/home/mark/sc/rand.file", O_CREAT | O_RDWR | O_TRUNC, USR_MODE);
  close_bin[close_bin_size++] = rand_data_file;
  if (rand_data_file < 0) {
    errorHandler(ERRMSG, 1);
  }

  for (int i = 0; i < 10; i++) {
    if (read(urandom, buff, BUFF_SIZE) < 0) {
      errorHandler(RDERRMSG, 3);
    }
    if (write(rand_data_file, buff, BUFF_SIZE) < 0) {
      errorHandler(WRERRMSG, 4);
    }
  }


  int sum = 0;
  char* hischar = malloc(sizeof(char));

  for (int i = 0; i < getLen(argv[1]); i++) {
    sum += (int)argv[1][i];
  }

  sum /= getLen(argv[1]);

  if (lseek(rand_data_file, sum, SEEK_SET) < 0) {
    errorHandler("Can't seek shit!", 69);
  }

  if (read(rand_data_file, hischar, sizeof(char)) < 0) {
    errorHandler(RDERRMSG, 3);
  }

  printf("Your character sir: %c\n", hischar);


  for (int i = 0; i < close_bin_size; i++) {
    int close_op = close(close_bin[i]);
    if (close_op < 0) {
      errorHandler(CLERRMSG, 1);
    }
  }

  return 0;
}


// Gets the length of a c-string by iterating until the null character is found
int getLen(char* s) {
  int i = 0;
  while ((int)s[i] != 0) {
    i++;
  }
  return i;
}


// Handles error messages and exits the program if they are severe
// A severe error is one whos retval = 0
void errorHandler(char* msg, int retval) {
  perror(msg);
  if (retval != 0) {
    exit(retval);
  }
}
