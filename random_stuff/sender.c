#include <stdio.h>
#include <signal.h>

#define MASK 127

int main() {
  int pid;
  printf("What is the pid of the process to communicate with?\n");
  scanf("%d", &pid);

  char msg = 69;
  for (int i = 0; i < 8; i++) {
    if (msg & MASK == 1) {
      kill(pid, SIGUSR2);
      printf("Sent a 1\n");
    } else {
      kill(pid, SIGUSR1);
      printf("Sent a 0\n");
    }
    msg = msg << 1;
    sleep(1);
  }
}
