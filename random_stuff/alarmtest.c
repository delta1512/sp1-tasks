#include <stdio.h>
#include <signal.h>


void mySleep(int seconds) {
  alarm(seconds);
  pause();
}


void alrmHdlr(int signum) {

}


int main() {
  signal(SIGALRM, alrmHdlr);

  printf("Hello \n");
  mySleep(3);
  printf("world!\n");

  return 0;
}
