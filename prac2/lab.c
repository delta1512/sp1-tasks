#include <stdio.h>
#include <stdlib.h>

#define LEN 10


typedef char DATA;
struct node
{
  DATA d;
  struct node* next;
};


void printLinkedList(struct node* startList);
struct node* sequentialLinkedList(int n);
struct node* reversedLinkedList(int n);
struct node* reverseList(struct node* startList, struct node* prevNode);
void deleteLinkedList(struct node* startList);
void swapNodes(struct node* a, struct node* b);


int main() {
  struct node* myLinkedList = sequentialLinkedList(LEN);
  printLinkedList(myLinkedList);
  struct node* reversedList = reversedLinkedList(LEN);
  printLinkedList(reversedList);
  printf("Reversing the 0-%d list: ", LEN-1);
  printLinkedList(reverseList(myLinkedList, myLinkedList));
  printf("Reversing the %d-0 list: ", LEN-1);
  printLinkedList(reverseList(reversedList, reversedList));

  return 0;
}


void printLinkedList(struct node* startList) {
  struct node* currentList = startList;
  while (currentList->next != (struct node*)0) {
    printf("%d ", currentList->d);
    currentList = currentList->next;
  }
  printf("%d \n", currentList->d);
}


struct node* sequentialLinkedList(int n) {
  // Begin creation of the linked list
  struct node* startList = (struct node*) malloc(sizeof(struct node));
  // create a temporary variable to fill the list
  struct node* currentList = startList;
  // Fix off-by-one bug
  currentList->d = 0;

  for (char i = 1; i < n; i++) {
    // Create a new node to prepare for the new iteration of the list
    currentList->next = (struct node*) malloc(sizeof(struct node));
    // Switch context to the new node
    currentList = currentList->next;
    // Place the value of the iteration in the list
    currentList->d = i;
  }
  // Return the pointer to the start of the list
  return startList;
}


struct node* reversedLinkedList(int n) {
  // Begin creation of the linked list
  struct node* startList = (struct node*) malloc(sizeof(struct node));
  // create temporary variables to fill the list
  struct node* prevNode = startList;

  for (char i = 0; i < n; i++) {
    // Place the value of the iteration in the list
    prevNode->d = i;
    // Create a new node to prepare for the new iteration of the list
    // This new iteration is the start
    startList = (struct node*) malloc(sizeof(struct node));
    // Set the new start to point to the previous one we made
    startList->next = prevNode;
    // Switch context to the new node
    prevNode = startList;
  }
  // Return the pointer to the start of the list
  // The start is actually the next item because of the off-by-one bug
  return startList->next;
}


// Recursive function that reverses a linked list
struct node* reverseList(struct node* startList, struct node* prevNode) {
  // If we come to the end of the list...
  if (startList->next == (struct node*)0) {
    // Swap the nodes and return the new start of the list
    swapNodes(startList, prevNode);
    return startList;
  } else {
    // Recurse to the next tuple of the list and temporarily store the result
    struct node* temp = reverseList(startList->next, startList);
    // If we are on any but the first recursion...
    if (startList != prevNode) {
      // Swap the nodes
      swapNodes(startList, prevNode);
    }
    // Return the new startList
    return temp;
  }
}


// For cases where b points to a, swap the nodes
void swapNodes(struct node* a, struct node* b) {
  // Store the next hop from a temporarily
  struct node* temp = a->next;
  // Set a to point to b
  a->next = b;
  // set b to point to whatever a was pointing to
  b->next = temp;
}
