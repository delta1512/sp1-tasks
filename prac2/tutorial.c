#include <stdio.h>

enum DAYS {mon=0, tue, wed, thu, fri, sat, sun};

int main() {
  printf("mon: %d\n", mon);
  printf("tue: %d\n", tue);
  printf("wed: %d\n", wed);
  printf("thu: %d\n", thu);
  printf("fri: %d\n", fri);
  printf("sat: %d\n", sat);
  printf("sun: %d\n", sun);

  return 0;
}

/*
PSEUDOCODE for linked list loop:
-------------------------------------

define data structure node containing {
  d of type char;
  and next of type node pointer
}

create the first node of the list as startList
set startList.d = 0
set currentList to point to startlist

FOR count=1 TO 10
  set currentList.next = new node
  set currentList = currentList.next
  set currentList.d = count
ENDFOR
*/
