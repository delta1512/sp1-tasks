#include <stdio.h>
#include <stdlib.h>


int factorial(int n);


int main(int argc, char* argv[]) {
  int n;

  printf("Enter a number: ");
  scanf("%d", &n);
  printf("%d! is %d\n", n, factorial(n));

  return 0;
}


int factorial(int n) {
  if (n == 1) {
    return 1;
  } else {
    return factorial(n-1)*n;
  }
}
