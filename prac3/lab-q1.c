#include <stdio.h>
#include <stdlib.h>


int factorial(int n);
int getLen(char* s);
int cstringToInt(char* s);


int main(int argc, char* argv[]) {
  if (argc < 2) {
    printf("Please specify a number to compute the factorial.\n");
    return 1;
  }
  int n = cstringToInt(argv[1]);

  printf("%d! is %d\n", n, factorial(n));

  return 0;
}


int factorial(int n) {
  if (n == 1) {
    return 1;
  } else {
    return factorial(n-1)*n;
  }
}


// Converts a c-string to an integer
int cstringToInt(char* s) {
  int final = 0;
  int strlen = getLen(s);
  int mag = 1;

  // Working from the last character to the first
  for (int i = strlen-1; i >= 0; i--) {
    // Each character is encoded ASCII so normalised to 0 (48 in base 10)
    // Then multiply by the order of magnitude
    final += ((int)s[i] - 48) * mag;
    // Move to the next decimal place
    mag *= 10;
  }

  return final;
}


// Gets the length of a c-string by iterating until the null character is found
int getLen(char* s) {
  int i = 0;
  while ((int)s[i] != 0) {
    i++;
  }
  return i;
}
