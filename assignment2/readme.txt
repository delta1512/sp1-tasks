UNIX-Style Multitasking Demonstration by Marcus Belcastro (19185398)


Compile and run using the following commands:
  gcc main.c
  ./a.out


// The solution logic is described for trivial functions whereas pseudocode is used for more complex functions

Solution logic for the main function:
  - The main function sets up 2 arrays, one which stores PIDs and another which stores the functions for each child to run.
  - The parent thread will iterate through the number of child processes and store their PID in the array.
  - The i'th will call the i'th element of the array of functions assigned to the children.
  - The parent will wait for all children to finish and print any errors.

Solution logic for child 1:
  - Iterate 10 times, collect the sum of the marks as floats
  - Divide the sum by 10 and print it

Solution logic for child 2:
  - Load the critical arguments into the argument array
  - Simply call the wc program using execvp() function

Solution pseudocode for child 3:
FUNCTION child3Proc(char array arg)
  Get the size of the file and create a buffer fileContent
  Create a buff of the same size+29 called finalContent

  Read the file into the fileContent buffer
  Copy the string to place at the start of the file to finalContent
  let finalSeek = 29
  let sTerm be the first char in the search term

  FOR count=0 TO the amount of bytes read
    IF char at fileContent[count] equals sTerm THEN
      let found = false
      let pos = count
      count = count + 1
      Iterate over sTerm while fileContent[count] equals sTerm[count] and if all characters are equal, set found = true
      IF found is true THEN
        copy the replacement term to finalContent
        finalSeek = finalSeek + length of replacement term
      ELSE
        copy the char at fileContent[pos] to finalContent
        finalSeek = finalSeek + 1
        count = pos + 1
      ENDIF
    ENDIF
    copy the char at fileContent[pos] to finalContent
    finalSeek = finalSeek + 1
  NEXT count

  Write the finalContent to the output file
END FUNCTION


Test plan:
1. child1: Test 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 (expected output: 1)
Notes: Success (Prints an additional 6 decimals of precision)
2. child1: Test 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 (expected output: 5.5)
Notes: Success
3. child1: Test -1,-10 (expected output: Should not accept any of these inputs)
Notes: Success (The program prompts for output after invalid data)
4. child2: Test a file with 1000 words (expected output: x 1000 x)
Notes: Success, tested using https://lipsum.com/ (23 1000 6692 file1)
5. child2: Test an empty file (expected output: 0 0 0)
Notes: Success (0 0 0 empty_file.txt)
6. child2: Test a file with 10 characters each on a newline (expected output: 10 10 20)
Notes: Success (10 10 20 newlines.txt)
7. child3: Test "Test examine test" (expected output: "This is the updated version.\nTest check test")
Notes: Success
8. child3: Test "examine the examiner examine." (expected output: "This is the updated version.\ncheck the checkr check.")
Notes: Success
9. child3: Test "" (expected output: "This is the updated version.\n")
Notes: Success
10. Test the program with 0 arguments (expected output: "Not enough parameters passed, 2 required.")
Notes: Success
11. Test the program with files that don't exist. (expected output: wc should notify that the file doesn't exist and child3 should exit with status 1)
Notes: Success


Limitations of the program:
 - Generalised fork()s are not exactly general. When increasing the number of child processes NUM_CHILDREN, you must add additional functions to void (*childFuncs[NUM_CHILDREN])(char*) in main().
 - Generalised search, replacement and beginning strings are not exactly general. You must append the lengths of the strings listed below whenever any #define strings are changed.
 - The program will replace any instance of the work "examine" it finds and hence words such as "examiner" will become "checkr".
 - The program may not work when the file size is approximately half of the computer's random-access memory and may cause the computer or program to crash.
 - The program only works with normal files or UNIX files that support an accurate file size metric.
