#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>

#define FILE_BUFF_SIZE 1024
#define BEGIN_STR "This is the updated version.\n"
#define BEGIN_STR_LEN 29
#define SEARCH_TERM "examine"
#define SEARCH_TERM_LEN 7
#define REPLACEMENT "check"
#define REPLACEMENT_LEN 5
#define NUM_CHILDREN 3

#define EXIT_STATUS_MASK 0xFF00
#define CORE_BIT_MASK 0x80
#define SIG_STATUS_MASK 0x7F



void child1Proc(char* arg);
void child2Proc(char* arg);
void child3Proc(char* arg);

void copyBuff(char* from, char* to, int startFrom, int tosize);
void addToBuff(char* buff, char* data, int dsize, int startFrom);
int readFile(char* f, char* fileContent);
void writeFile(char* f, char* fileContent, int bytesAwaitingWrite);
int min(int a, int b);


// Argv structure: ./a.out [arg1] [arg2] ...
int main(int argc, char* argv[]) {
  int children[NUM_CHILDREN];
  // Create an array of function pointers to iterate through
  void (*childFuncs[NUM_CHILDREN])(char*) = {
    child1Proc, child2Proc, child3Proc
  };
  // Do sanitisation checks
  if (argc < NUM_CHILDREN-1) {
    printf("Not enough parameters passed, %d required.\n", NUM_CHILDREN-1);
    return 1; // Return unsuccessful
  }

  // Set the current context to the first child
  void (*context)(char*) = NULL;

  // Iterate through all the child processes we want to make
  for (int i = 0; i < NUM_CHILDREN; i++) {
    // Fork a child process
    children[i] = fork();
    // Check if it was a success
    if (children[i] == -1) {
      perror("Failed to start a fork. Exiting...");
      exit(1);
    }
    // If the current context is the child process
    if (children[i] == 0) {
      // Load the function to perform
      context = childFuncs[i];
      // Execute the function
      context(argv[i]);
      // Do not spawn anymore children
      break;
    }
  }

  // If the current process is the parent process
  if (context == NULL) {
    // Wait for all the children and print all of their statuses
    for (int i = 0; i < NUM_CHILDREN; i++) {
      int status = 0;
      int rval = wait(&status);
      int exitStat = (status & EXIT_STATUS_MASK) >> 8;
      int coreDump = (status & CORE_BIT_MASK) >> 7; // Normalise to 1 or 0
      int sig = status & SIG_STATUS_MASK;
      printf("Exit rval=%d, status=%d, core=%d, sig=%d\n",
        rval, exitStat, coreDump, sig
      );
    }
  }

  return 0;
}


void child1Proc(char* arg) {
  float sum = 0.0;
  float input = 0.0;

  // Sleep for a small time to improve the user friendliness
  sleep(1);

  for (int i = 0; i < 10; i++) {
    do {
      printf("Input mark:\n");
      scanf("%f", &input);
    } while (input < 0.0); // Sanitisation of inputs
    sum += input;
  }

  printf("The average score is: %f\n", sum/10.0);
}


void child2Proc(char* arg) {
  // Load the arguments like you would with a normal process executed at the command line
  char* argv[3];
  argv[0] = "/usr/bin/wc";
  argv[1] = arg;
  argv[2] = NULL;
  execvp("/usr/bin/wc", argv);
  // If the execvp process continues, then an error has occurred
  perror("wc program ran into an error.\n");
  // Exit with an error
  exit(1);
}


void child3Proc(char* arg) {
  // Collect the file size to determine how big of a buffer we need to make
  struct stat fileInfo;
  stat(arg, &fileInfo);
  // Create a space in memory to store the entire file
  char* fileContent = malloc(fileInfo.st_size);

  // Start by reading all the data of the file into memory
  int fileSize = readFile(arg, fileContent);

  // Create a space in memory to store the edited file
  // +BEGIN_STR_LEN for the additional "This is the updated version.\n" at the start
  char* finalContent = malloc(fileSize+BEGIN_STR_LEN);

  // Create some strings ready to handle
  char beginning[] = BEGIN_STR;
  char term[] = SEARCH_TERM;
  char replace[] = REPLACEMENT;
  // Emplace the required string at the start of the file
  addToBuff(finalContent, beginning, BEGIN_STR_LEN, 0);
  int finalSeek = BEGIN_STR_LEN;

  // Go through the data to find instances of "examine"
  for (int i = 0; i < fileSize; i++) {
    // If the current character is the first term
    if (fileContent[i] == term[0]) {
      int found = 0;
      int pos = i; // Save the current position in case we don't find it
      i++; // After finding the first letter, move forward
      // Iterate through the entire search term
      for (int count = 1; count < SEARCH_TERM_LEN; count++) {
        //printf("cont: %c ---- term %c\n", fileContent[i], term[count]);
        // If we ever come across a different char to the term
        if (fileContent[i] != term[count]) {
          break; // Stop search looking for the string
        // If we are at the end of the loop, flag that we found the exact term
        } else if (count == SEARCH_TERM_LEN-1) {
          found = 1;
        }
        i++; // Move through the real file contents at the same time
      }
      if (found) {
        addToBuff(finalContent, replace, REPLACEMENT_LEN, finalSeek);
        finalSeek += REPLACEMENT_LEN;
      } else {
        finalContent[finalSeek] = fileContent[pos];
        finalSeek++;
        i = pos+1;
      }
    }
    // For each character, copy it to the final edited file
    finalContent[finalSeek] = fileContent[i];
    finalSeek++;
  }
  // We don't need the file content anymore, garbage collect it
  free(fileContent);

  // Write the edited file to the file system
  writeFile(arg, finalContent, finalSeek);

  // Garbage-collect the copied and edited file
  free(finalContent);
}



// Copies data from one buffer to another starting from startFrom and copying
// a total of tosize bytes of data.
// This function does not check sizes of the buffers and will not catch a
// buffer overflow.
void copyBuff(char* from, char* to, int startFrom, int tosize) {
  for (int i = 0; i < tosize; i++) {
		to[i] = from[startFrom+i];
	}
}


// Copies the chars from data into buff starting at startFrom
// Does not check whether there will be a buffer overflow
void addToBuff(char* buff, char* data, int dsize, int startFrom) {
  for (int i = 0; i < dsize; i++) {
		buff[startFrom+i] = data[i];
	}
}


// Reads a file f into the buffer fileContent and returns the amount of bytes read
// fileContent must be initialised before reading the file and must be of the
// appropriate size.
int readFile(char* f, char* fileContent) {
  int fd = open(f, O_RDONLY);
  if (fd < 0) {
    perror("Failed to open file for reading in child 3. Exiting...\n");
    exit(1);
  }

  int totalBytesRead = 0;
  int currentBytesRead;
  do {
    // Create a buffer to store a block of data
    char* buff = malloc(sizeof(char)*FILE_BUFF_SIZE);
    // Read the data
    currentBytesRead = read(fd, buff, FILE_BUFF_SIZE);
    // Add it to the big buffer for the whole file
    addToBuff(fileContent, buff, currentBytesRead, totalBytesRead);
    totalBytesRead += currentBytesRead;
    // Garbage-collect the temporary buffer
    free(buff);
  } while (currentBytesRead > 0);

  if (close(fd)) {
    perror("Failed to close file from reading in child 3. Exiting...\n");
    exit(1);
  }

  return totalBytesRead;
}


// Writes the content of fileContent to file f given bytesAwaitingWrite number
// of bytes to write. Does not automatically free  fileContent buffer.
// Will completely overwrite the provided file with the new content.
void writeFile(char* f, char* fileContent, int bytesAwaitingWrite) {
  // O_TRUNC to create a new file
  int fd = open(f, O_WRONLY | O_TRUNC);
  if (fd < 0) {
    perror("Failed to open file for writing in child 3. Exiting...\n");
    exit(1);
  }

  int totalBytesWritten = 0;
  do {
    // Figure out how many bytes we have to write
    int bytesToWrite = min(FILE_BUFF_SIZE, bytesAwaitingWrite);
    // Create a temporary buffer
    char* buff = malloc(sizeof(char)*bytesToWrite);
    // Copy the content of our edited file to the new buffer
    copyBuff(fileContent, buff, totalBytesWritten, bytesToWrite);
    // Write the block of data
    if (write(fd, buff, bytesToWrite) == -1) {
      perror("Failed to write back to the file. Stopping all write operations...\n");
      free(buff);
      break;
    }
    bytesAwaitingWrite -= bytesToWrite;
    totalBytesWritten += bytesToWrite;
    // Garbage-collect the temporary buffer
    free(buff);
  } while (bytesAwaitingWrite > 0);

  if (close(fd)) {
    perror("Failed to close file from writing in child 3. Exiting...\n");
    exit(1);
  }
}


int min(int a, int b) {
  if (a < b) {
    return a;
  } else {
    return b;
  }
}
