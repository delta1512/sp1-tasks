/** ls1.c
 **   purpose  list contents of directory or directories
 **   action   if no args, use .  else list files in args
 **/
#include  <stdlib.h>
#include	<stdio.h>
#include	<sys/types.h>
#include 	<sys/stat.h>
#include	<dirent.h>

void do_ls(char []);
int getLen(char* s);
void concatStr(char* a, char* b, char** buff);
void show_stat_info(struct stat *buf);


int main(int ac, char *av[])
{
	if ( ac == 1 ) {
		do_ls( "." );
	} else {
		while ( --ac ) {
			printf("%s:\n", *++av );
			do_ls( *av );
		}
	}
}


void do_ls( char dirname[] )
/*
 *	list files in directory called dirname
 */
{
	DIR		*dir_ptr;		/* the directory */
	struct dirent	*direntp;		/* each entry	 */

	// Sanitise the directory by adding a / to the end
	char* sanitisedDir;
	concatStr(dirname, "/", &sanitisedDir);

	if ( ( dir_ptr = opendir( dirname ) ) == NULL ) {
		fprintf(stderr,"ls1: cannot open %s\n", dirname);
	} else {
		while ( ( direntp = readdir( dir_ptr ) ) != NULL ) {
			// Initialise the full path string and stat info
			char* path;
			struct stat info;
			// Get the full path string
			concatStr(sanitisedDir, direntp->d_name, &path);
			// Collect the stat() data
			stat(path, &info);
			// Print the directory entry file name
			printf("%s\n", direntp->d_name );
			// Print the inode of the file
			printf("\tinode:\t %d\n", direntp->d_ino);
			// Show all the stat info like fileinfo.c
			show_stat_info(&info);
			// Delete the string to save memory
			free(path);
		}
		closedir(dir_ptr);
	}
}


// Function from fileinfo.c
void show_stat_info(struct stat *buf)
/*
 * displays some info from stat in a name=value format
 */
{
    printf("\tmode:\t %o\n", buf->st_mode);         /* type + mode */
    printf("\tlinks:\t %d\n", buf->st_nlink);        /* # links     */
    printf("\tuser:\t %d\n", buf->st_uid);          /* user id     */
    printf("\tgroup:\t %d\n", buf->st_gid);          /* group id    */
    printf("\tsize:\t %d\n", buf->st_size);         /* file size   */
    printf("\tmodtime: %d\n", buf->st_mtime);        /* modified    */
}


// Concatenates strings a and b into the buffer
// The double-reference string means that buffer manipulations occur within
// the function and the buffer must be freed outside the function where necessary
void concatStr(char* a, char* b, char** buff) {
	int an = getLen(a);
	int bn = getLen(b);
	// minus 1 because there are 2 NULL chars
	*buff = (char*) malloc(sizeof(char) * (an+bn-1));

	// Copy the strings to the buffer
	for (int i = 0; i < an; i++) {
		(*buff)[i] = a[i];
	}
	for (int i = 0; i < bn; i++) {
		(*buff)[i+an] = b[i];
	}
}


// Gets the length of a c-string by iterating until the null character is found
int getLen(char* s) {
  int i = 0;
  while ((int)s[i] != 0) {
    i++;
  }
  return i;
}
