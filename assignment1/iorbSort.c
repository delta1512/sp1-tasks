#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

#include "iorbSort.h"


// Sorts the list using an in-place selection sort
IORB* sortList(IORB* head, int(*prio)(int)) {
  printf("---------- STARTED SORT LIST ----------\n");
  // The pointer to the start of the final sorted list
  IORB* finalHead;
  // The head of where the sorted portion of the list ends
  IORB* currentEnd = (IORB*) NULL;
  // The head of where to start comparing for the next iteration
  // Is the node directly after currentEnd
  IORB* nextStart = head;

  // While there are still more nodes to order
  while (nextStart != (IORB*) NULL) {
    // Set up the minimum variables to track
    IORB* minIorb = (IORB*) NULL;
    // Tracks the node immediately behind the minimum node
    // so that it can link to the next node when the sort happens
    IORB* minPrev = currentEnd;
    int minPri = INT_MAX;
    // Tracks the current and previous nodes as the loop iterates
    IORB* prev = (IORB*) NULL;
    IORB* current = nextStart; // Start from unordered nodes

    // Loop through all the unordered nodes
    while (current != (IORB*) NULL) {
      // Collect and compare priorities
      int currentPri = prio(current->base_pri);
      if (currentPri < minPri) {
        // If new minimum, update the variables
        minIorb = current;
        minPrev = prev;
        minPri = currentPri;
      }
      // Iterate to the next unordered node
      prev = current;
      current = current->link;
    }

    // Perform the move to the front (end of sorted) of the list...
    // If the node has a parent
    if (minPrev != (IORB*) NULL) {
      // Make the parent bypass the current node and point to the next
      minPrev->link = minIorb->link;
    }

    // when we start from fresh
    if (currentEnd == (IORB*) NULL) {
      // Set the head of the sorted list to be the first minimum node
      finalHead = minIorb;
    } else {
      // Add a new minimum to the end of the sorted list
      currentEnd->link = minIorb;
    }

    currentEnd = minIorb;

    // Only iterate through nextStart if it has been selected as a minimum
    if (currentEnd == nextStart) {
      nextStart = nextStart->link;
    }
  }

  return finalHead;
}


// Prints only a single IORB node
void printNode(IORB* node, int(*prio)(int)) {
  printf("Node:\n");
  printf("base_pri: %d\n", node->base_pri);
  printf("sched_pri: %d\n", prio(node->base_pri));
  printf("filler: %s\n\n", node->filler);
}


// Prints the whole list from a pointer to the head
void displayList(IORB* head, int(*prio)(int)) {
  printf("---------- PRINTING LIST ----------\n");
  IORB* current = head;
  printNode(current, prio);

  while (current->link != (IORB*) NULL) {
    current = current->link;
    printNode(current, prio);
  }
}


// Free all the memory in the list
// Recursive function that deletes one node and recurses if the pointer is null
void deleteList(IORB* head) {
  if (head->link != (IORB*) NULL) {
    deleteList(head->link);
    free(head);
  }
}
