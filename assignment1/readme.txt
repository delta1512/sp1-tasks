IORB scheduling simulator by Marcus Belcastro (19185398)


Compile and run using the following commands:
  gcc *.c
  ./a.out


Solution pseudocode of the sort function:
FUNCTION sortList(linked list pointer L)
  Track the following pointers:
    - the start of the sorted sublist
    - the end of the sorted sublist
    - the start of the unsorted sublist (starts as the start of L)
  FOR every element in the list (elem)
    Track the following pointers:
      - the minimum IORB
      - the node previous to the min IORB
      - the current node (set to elem)
      - the node before current
    Let minPri be the minimum scheduling priority of the search
    FOR every element in the unsorted list (current)
      IF scheduling prio. of current < minPri THEN
        set current to be the new min IORB
        set the node before current to be the previous min IORB
        minPri = scheduling prio. of current
      ENDIF
    NEXT current

    IF the min IORB has a parent node THEN
      point the parent to the child of the min IORB
    ENDIF

    IF this is the first iteration of elem THEN
      set the start of the final sorted list to be the min IORB
    ELSE
      link the end of the sorted sublist to the min IORB
    ENDIF

    set the end of the sorted sublist to be the min IORB

    only move to next elem if the current elem is equal to the min IORB
  NEXT elem
END FUNCTION


Tests:
1. Test random list with a high probability (0.9)
  + success (sorts correctly with 28 elements)
2. Test random list with a low probability (0.5)
  + success (sorts correctly with 1 elements)
3. Test a custom list with 0 elements (should not allow it)
  + success (refused to accept input)
4. Test a custom list with 1 element
  + success (sorts successfully as it is the only element)
5. Test a custom list with 10 elements
  + success (sorts correctly and similar numbers are positioned next to each other as expected)
6. Test the modulo priority funtion with negative numbers
  + success (sorts correctly, modulo functions as expected)
7. Test the double priority function with negative numbers
  + success (sorts correctly, double function works correctly even with negatives)


Limitations of the program:
- The IORB list must have at least 1 element in it
- The choice of priority functions includes only the following:
  - Identity function (returns the base_pri with no manipulations)
  - Double function (returns double the base_pri)
  - Modulo function (returns the modulo of base_pri with a large prime MOD_VAL)
  - Negation function (returns the negative of the base_pri)
- Cannot add a custom filler, only fills the filler with random data in a specified range
