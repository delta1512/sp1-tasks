#define IORB_FILLER_SIZE 100
#define LINK_PROB 0.825
#define MAX_PRI 5000

typedef struct iorb {
  int base_pri;
  struct iorb *link;
  char filler[IORB_FILLER_SIZE];
} IORB;

IORB* sortList(IORB* head, int(*prio)(int));
void printNode(IORB* node, int(*prio)(int));
void displayList(IORB* head, int(*prio)(int));
void deleteList(IORB* head);
