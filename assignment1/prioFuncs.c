#include "prioFuncs.h"


int identityF(int priority) {
  return priority;
}


int doubleF(int priority) {
  return priority + priority;
}


int moduloF(int priority) {
  return priority % MOD_VAL;
}


int negateF(int priority) {
  return -priority;
}
