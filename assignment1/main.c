#include <stdio.h>
#include <time.h>
#include <stdlib.h>

#include "iorbSort.h"
#include "prioFuncs.h"


int getRand(int a, int b);
double getRandUniform();
void fillFiller(char* filler);
IORB* buildList();
IORB* makeCustomList();


int main() {
  srand(time(NULL));

  printf("--- IORB simulator by Marcus Belcastro (19185398) ---\n\n");

  int end = 0;
  do {
    int choice = 0;
    IORB* list;
    // Get a list either randomly or deterministically
    do {
      printf("Random list (1) or custom list (2): ");
      scanf("%d", &choice);
    } while (choice > 2 || choice < 1);

    switch (choice) {
      case 1:
        list = buildList();
        break;
      case 2:
        list = makeCustomList();
        break;
    }

    int fChoice = 0;
    int(*priChoice)(int); // Pointer to priority function
    // Get the priority function to use for scheduling
    do {
      printf("Identity function (1)\n");
      printf("Double function (2)\n");
      printf("Modulo function (3)\n");
      printf("Negation function (4)\n");
      printf("Choose your scheduling algorithm: ");
      scanf("%d", &fChoice);
    } while (fChoice < 1 || fChoice > 4);

    switch (fChoice) {
      case 1:
        priChoice = identityF;
        break;
      case 2:
        priChoice = doubleF;
        break;
      case 3:
        priChoice = moduloF;
        break;
      case 4:
        priChoice = negateF;
        break;
    }

    // Print and sort the list
    printf("Here is your list...\n");
    displayList(list, priChoice);

    list = sortList(list, priChoice);

    printf("Here is your sorted list...\n");
    displayList(list, priChoice);

    // Free the used memory
    deleteList(list);

    printf("Quit (99)? or type anything else to continue: ");
    scanf("%d", &end);
  } while (end != 99);

  return 0;
}


// Fills the filler field with random characters
void fillFiller(char* filler) {
  for (int i = 0; i < IORB_FILLER_SIZE; i++) {
    filler[i] = (char) getRand(32, 126);
  }
}


// Build a random list
// The LINK_PROB is the probability that the list will continue to be built
// The returned pointer must be freed using deleteList()
// The returned pointer points to the head of the list with at least 1 element
IORB* buildList() {
  IORB* head = (IORB*) malloc(sizeof(IORB));
  IORB* current = head;
  current->base_pri = getRand(0, MAX_PRI);
  fillFiller(current->filler);

  while (getRandUniform() <= LINK_PROB) {
    // Create a new node and allocate memory
    current->link = (IORB*) malloc(sizeof(IORB));
    // Move to the new node
    current = current->link;
    // Randomise the data
    current->base_pri = getRand(0, MAX_PRI);
    fillFiller(current->filler);
    // Ensure explicit NULL value of the link field for the last node
    current->link = (IORB*) NULL;
  }

  return head;
}


// Builds a list of IORBs from a user-defined base_pri and length
// The returned pointer must be freed using deleteList()
IORB* makeCustomList() {
  int size = 0;
  // Get the head to the start of the list and prepare memory
  IORB* head = (IORB*) malloc(sizeof(IORB));
  IORB* current = head;
  // Query the number of elements
  do {
    printf("How many elements? ");
    scanf("%d", &size);
  } while (size < 1);

  // Query the priority and build the list based on the specified length
  for (int i = 0; i < size; i++) {
    // Fetch base_pri
    printf("What is the base priority of IORB %d? ", i+1);
    scanf("%d", &(current->base_pri));
    // Randomise the filler
    fillFiller(current->filler);

    if (i != size-1) {
      // If it is not the last node, prepare a new node
      current->link = (IORB*) malloc(sizeof(IORB));
      current = current->link;
    } else {
      // Ensure explicit NULL value of the link field for the last node
      current->link = (IORB*) NULL;
    }
  }

  return head;
}


// Gets a random number between a and b (inclusive)
int getRand(int a, int b) {
  return rand() % (b-a+1) + a;
}


// Gets a random number between 0 and 1
double getRandUniform() {
  return (double) rand() / (double) RAND_MAX;
}
