#include <stdio.h>

main() {
  int a, b, c;

  printf("Input 3 numbers: ");
  scanf("%d", &a);
  scanf("%d", &b);
  scanf("%d", &c);

  // Prints the result of the function call to get the largest 3 numbers
  printf("\nThe largest number is: %d\n", get_largest_of_3(a, b, c));

  exit(0);
}


int max(int a, int b) {
  // If a is larger than b, then a is larger
  if (a > b) {
    return a;
  }
  // Else b is larger
  return b;
}


int get_largest_of_3(int a, int b, int c) {
  // Returns a double max function call between the 3 variables
  // "choos themaximum between a and b; and c"
  return max(max(a, b), c);
}
