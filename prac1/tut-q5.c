# include <stdio.h>
/*print Fahrenheit-Celsius table for fah=0,20..,200*/

main() {
  int fah, cel;
  int lower, upper, step;

  lower=0;  /*lower limit of the temperature table */
  fah = lower; // assign the initial value to fah
  upper=400;  /* upper limit */
  step=20;  /*step size */

  do {
    // Repeat the processes from the previous program
    cel=5*(fah-32)/9;
    printf("%d\t%d\n", fah, cel);
    // Increment fah
    fah += step;
  } while (fah <= upper); // Perform the comparison condition from the for loop

  exit(0);
}
