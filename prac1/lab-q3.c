#include <stdio.h>

main() {
  int a, b, c, final;

  // Get user input into a, b and c
  printf("Input 3 numbers: ");
  scanf("%d", &a);
  scanf("%d", &b);
  scanf("%d", &c);

  // If a is bigger than b
  if (a > b) {
    // But if c is larger than a
    if (c > a) {
      // C is the biggest
      final = c;
    } else {
      // Otherwise a is the biggest
      final = a;
    }
  // If b is bigger
  } else {
    // And is bigger than c
    if (b > c) {
      // Then b is the biggest
      final = b;
    } else {
      // Otherwise c is bigger
      final = c;
    }
  }

  printf("\nThe largest number is: %d\n", final);

  exit(0);
}
